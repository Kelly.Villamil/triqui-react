//importar una biblioteca
import React from 'react' //como include
//importe a React desde la biblio react
import ReactDOM from 'react-dom'
//importar eso desde la biblio reactdom
import './index.css' //./ significa que esta en la misma carpeta
import { findRenderedDOMComponentWithClass } from 'react-dom/test-utils';

function Casilla(props) { //casilla se vuelve una funcion
    return ( //esta funcion retorna una casilla con esos atributos
        <button className='Casilla' onClick={props.onClick}>
            {props.value}
        </button>
    );
}
  
class Tablero extends React.Component { //esta clase define el tablero y sus metodos

    constructor(props){ //el constructor siempre necesita las propiedades
        super(props); //siempre se pone pero no se por que
        this.state={ //el estado
            casillas: Array(9).fill(null),//las 9 casillas en un arreglo con estado null por defecto
            Xsigue: true, //el estado del Xsigue, es para saber si sigue x u o, funciona con un booleano e inicia en true porque sigue x
        };
    }

    hacerClick(i){ //este metodo pone el estado de las casillas basandose en Xsigue en cada boton que demos clck
        const casillas = this.state.casillas.slice(); //se crea una constante casillas en la que se determina el estado del arreglo
            if(ganador(casillas) || casillas[i]){ //evaulua si ya gano alguno llamando la funcion ganador y viendo que rentorna (cuando esta funcion no retorna nada) la primera condicion no se cumple
                return; //impide que sigan jugando
            }
        casillas[i]=this.state.Xsigue ? 'X' : 'O' ; //si el if es falso la casilla clikeada pone lo que este en el estado de Xsigue 
        this.setState({ //creo que envia el estado
            casillas: casillas, //en casillas le actualiza  el X u O que hay en cada una
            Xsigue: !this.state.Xsigue, //invierte el booleano del Xsigue para poder cambiar de turnos
        });
    }

    rendercasilla(i) { //este metodo devuelve un objeto de la funcion casilla, con su valor i y cuando se da clic en ella llama al metodo hacer click
      return (
      <Casilla value={this.state.casillas[i]} onClick={() => this.hacerClick(i) }/>
                ); //envio i
    }
  
    render() { //renderiza
      const gana = ganador(this.state.casillas);//llaman la funcion ganador
      let estado;//estado
        if(gana){//si la funcion retorna algo
            estado = 'El ganador es: ' + gana;
        } else {//si no retorna nada sigue el otro
            estado = 'El siguiente jugador es: ' + (this.state.Xsigue ? 'X':'O');
        }
        
      return ( //aqui se retornan las 9 casillas cada una en su division
        <div>
            <div className="status">{estado}</div>
            <div className="Renglon">
                {this.rendercasilla(0)}
                {this.rendercasilla(1)}
                {this.rendercasilla(2)}
            </div>
            <div className="Renglon">
                {this.rendercasilla(3)}
                {this.rendercasilla(4)}
                {this.rendercasilla(5)}
            </div>
            <div className="Renglon">
                {this.rendercasilla(6)}
                {this.rendercasilla(7)}
                {this.rendercasilla(8)}
            </div>
        </div>
      );
    }
  }
  
  class Juego extends React.Component { //aqui esta el juego
    render() { //renderiza
      return ( //devuelve
        <div className="Juego">
            <div className="Juego-Tablero">
                <Tablero />
            </div>
            <div className="Juego-info"></div>
        </div>
      ); //division juego, division tablero, division info
    }
  }
  

  function ganador(casillas) { //esta funcion decide si ya se gano
    const lineas = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ]; //todas las posiciones posibles para que gane algun jugador
    for (let i = 0; i < lineas.length; i++) { //recorre las posiciones
      const [a, b, c] = lineas[i]; //declara una constante y le asigna lo que hay en lineas en la posicion i
      if (casillas[a] && casillas[a] === casillas[b] && casillas[a] === casillas[c]) { //compara si ya se gano en esa posicion
        return casillas[a]; //si si retorna el jugador que gano
      }
    } //finaliza el ciclo
    return null; //si nadie ha ganado no retorna nada
  }

//==================================================================================

  ReactDOM.render( //aqui se renderiza todoo
    <Juego/>,
    document.getElementById('root')
  ); //se renderiza juego, en el elemento root del documento
  